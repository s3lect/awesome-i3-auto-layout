#!/usr/bin/env python3

import getopt
import argparse
import os
import sys
import tempfile
import subprocess
import json
from pathlib import Path
from i3ipc import Connection, Event
import time
from functools import partial, wraps


this = sys.modules[__name__]
this.num_windows_tabbed = 5


class ThrottleDecorator(object):
  def __init__(self,func,interval):
    self.func = func
    self.interval = interval
    self.last_run = 0
  def __get__(self,obj,objtype=None):
    if obj is None:
        return self.func
    return partial(self,obj)
  def __call__(self,*args,**kwargs):
    now = time.time()
    if now - self.last_run < self.interval:
        return
    else:
        self.last_run = now
        return self.func(*args,**kwargs)

def Throttle(interval):
  def applyDecorator(func):
    decorator = ThrottleDecorator(func=func,interval=interval)
    return wraps(func)(decorator)
  return applyDecorator



def get_stack(window_count, split):
    percentages = [1.0 / window_count] * window_count
    elements = []
    for p in percentages:
        elements.append(node(p, split, True, False))
    return [{"layout": split, "type": "con", "nodes": elements}]

def append_layout(i3, layout_dict, window_count):
    """Apply a layout from this layout class"""
    tf = tempfile.NamedTemporaryFile(suffix=".json")
    tf.write(json.dumps(layout_dict, indent=4).encode("utf-8"))
    tf.flush()
    file_path = str(Path(tf.name).absolute())
    i3.command(f'append_layout {file_path}')
    tf.close()

def get_json_stacked(window_count):
    return node(
        1,
        "splith",
        False,
        [node(0.5, "splitv", True, []), get_stack(window_count - 1, "splitv")],
    )

def get_json_tabbed(window_count):
    return node(
        1,
        "splith",
        False,
        [node(0.5, "tabbed", True, []), get_stack(window_count - 1, "tabbed")],
    )

def get_active_window():
    return (
        subprocess.check_output(["xdotool", "getactivewindow"]).decode("utf-8").strip()
    )

def get_window_ids():
    """use xprop to list windows on current screen.
    Couldn't find out how to get the right ids from i3ipc.
    id is con_id, but we need x11 id
    Sorry, this probably means this won't work on wayland & sway.
    """

    desktop = subprocess.check_output(
        ["xprop", "-notype", "-root", "_NET_CURRENT_DESKTOP"]
    ).decode("utf-8", errors="replace")
    desktop = desktop[desktop.rfind("=") + 2 :].strip()
    try:
        res = subprocess.check_output(
            [
                "xdotool",
                "search",
                "--all",
                "--onlyvisible",
                "--desktop",
                desktop,
                "--class",
                "",
            ]
        ).decode("utf-8", errors="replace")
        return res.strip().split("\n")
    except Exception as e:
        return False

def node(percent, layout, swallows, children):
    result = {
        "border": "pixel",
        "floating": "auto_off",
        "percent": percent,
        "type": "con",
        "layout": layout,
    }
    if swallows:
        result["swallows"] = ([{"class": "."}],)
    if children:
        result["nodes"] = children
    return result

def focus_window(i3, id):
    i3.command(f'[con_id="{id}"] focus')

# def get_workspace(i3, window_id):
#     def finder(root, workspace):
#         if root.type == 'workspace':
#             workspace = root
#         if root.id == window_id:
#             return workspace
#         for node in root.nodes:
#             res = finder(node, workspace)
#             if res:
#                 return res
#         return None
#     return finder(i3.get_tree(), None)

# def find_parent(i3, window_id):
#     def list_ids(root, ids):
#         if not len(root.nodes):
#             return ids
#         ids += [{"pid": c.pid, "id": c.id} for c in root.nodes] 
#         for node in root.nodes:
#             return list_ids(node, ids)
#     get_workspace(id, window_id)
#     print("workspace", workspace.name)
#     ids = list_ids(workspace, []);
#     return ids

def get_non_floating_windows(i3):
    def finder(root, index):
        if root.window:
            index[root.window] = {"id": root.id, "floating": root.floating}
        if len(root.nodes):
            for node in root.nodes:
                res = finder(node, index)
        return index
    return finder(i3.get_tree(), {})

@Throttle(1) # do not accept another event for the next 1 second
def set_layout(i3, event):
    """
        Set the layout/split for the currently
        focused window to either vertical or
        horizontal, depending on its width/height
    """
    
    # windows = get_window_ids()
    # print("windows", windows)
    # print("tree", )
    # return
    
    if event and (event.container.floating == 'user_on' or event.container.floating == 'auto_on'):
        return

    non_floating_windows = get_non_floating_windows(i3)
    # print("non_floating_windows", non_floating_windows)
    # print("get_window_ids()", get_window_ids())
    # windows = [window_id for window_id in get_window_ids() if not (window_id in non_floating_windows)]
    # print("non floating windows", windows)
   
    all_windows = get_window_ids()
    # Filter out windows that are not in the i3 tree, they are floating
    windows = [x for x in all_windows if int(x) in non_floating_windows]
    if windows == False:
        print('Could not get windows, do not do anything.')
        return
    num_windows_tabbed = this.num_windows_tabbed
    if not event:
        num_windows_tabbed -= 1
    window_count = len(windows)
    if window_count < 2:
        return
    if (window_count >= num_windows_tabbed):
        layout_dict = get_json_tabbed(window_count)
    else:
        layout_dict = get_json_stacked(window_count)
    
    dry_run = False
    if dry_run:
        print(json.dumps(layout_dict, indent=4))
        return

    # move the current window to the left
    # workspace = get_workspace(i3, event_container_id)
    # if workspace and workspace.nodes: # delete has no current window and thus no workspace
    #     print("workspace.nodes", workspace.nodes, workspace.nodes[0].name)
    #     while workspace.nodes[0].nodes[0].id != event_container_id:
    #         i3.command('move.left')
    #         workspace = get_workspace(i3, event_container_id)

    append_layout(i3, layout_dict, window_count)
    # we unmap and map all at once for speed.
    unmap_cmd = ["xdotool"]
    map_cmd = ["xdotool"]
    for window_id in windows:
        unmap_cmd.append("windowunmap")
        map_cmd.append("windowmap")
        unmap_cmd.append(str(window_id))
        map_cmd.append(str(window_id))
    # force i3 to swallow these windows.
    subprocess.check_call(unmap_cmd)
    subprocess.check_call(map_cmd)
    if event:
        focus_window(i3, event.container.id)

def listen():
    """
        listen for window focus
        changes and call set_layout when focus
        changes
    """
    i3 = Connection()
    i3.on(Event.WINDOW_NEW, set_layout)
    i3.on(Event.WINDOW_CLOSE, set_layout)
    i3.main()

# Use this for running the script direcly
def run_once():

    i3 = Connection()
    set_layout(i3, None)
    

def main():
    parser = argparse.ArgumentParser(description='''Create an i3 two pannel layout with one window on the left and stacking or tabbed layout on the right. If no argument is passed the layout will be applyed once.''')
    parser.add_argument('-l', '--listen', 
        action='store_true',
        help='listen to new and close window events and apply the layout'
    )
    parser.add_argument('-w', '--windows', 
        type=int,
        default=5,
        help='number of windows when when tabbed layout is used (default is 5)'
    )
    args = parser.parse_args()
    if args.windows:
        this.num_windows_tabbed = args.windows
    
    if args.listen:
        listen()
    else:
        run_once()


if __name__ == "__main__":
    main()

