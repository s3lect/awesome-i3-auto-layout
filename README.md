# awesome-auto-layout i3wm layout script

Automatically create an awesome layout for your i3 windows.

```
> ./awesome-auto-layout -h
usage: awesome-auto-layout [-h] [-l] [-w WINDOWS]

Create an i3 two pannel layout with one window on the left and stacking or tabbed layout on the right. If no argument is passed the layout will be applyed once.

optional arguments:
  -h, --help            show this help message and exit
  -l, --listen          listen to new and close window events and apply the layout
  -w WINDOWS, --windows WINDOWS
                        number of windows when when tabbed layout is used (default is 5)
```


The script will automatically create a two panel layout that has one window on the left and a stack of windows on the right side. If the number of the stacked windows become too large it will switch to a tabbed layout on the right panel.

```
+------+ +------+  +------+     +------+ +------+     +-------+ 2 3 4 5
|      | |      |  |      |     |      | |      |     |       | +------+
|      | |      |  |      |     |      | |   2  |     |       | |      |
|      | |      |  |      |     |      | |      |     |       | |      |
|   1  | |   2  |  |   3  | +-> |   1  | +------+ +-> |   1   | |      |
|      | |      |  |      |     |      | +------+     |       | |      |
|      | |      |  |      |     |      | |      |     |       | |      |
|      | |      |  |      |     |      | |   3  |     |       | |      |
+------+ +------+  +------+     +------+ +------+     +-------+ +------+
```

## Install

You need to have `python3` installed and the i3ipc python module

```
pip3 install i3ipc
```

now you can either run the script with a keyboard shortcut or you can run it in listen mode to always apply the layout when new windows are created or closed. Add the following to you i3 config:

For the listen mode
```
exec --no-startup-id /path/to/awesome-auto-layout -l
```

For applying the layout once
```
bindsym $mod+l exec /path/to/awesome-auto-layout
```

## Issues

The focus of the active window still has some trouble, any help is welcome to improve that.

## Motivation

I recently switched from [awesome wm](https://awesomewm.org/) to regolith i3wm, but I missed the auto layout feature since I only used one window layout all the time anyways. Searching for an i3 script that could achieve that I was not 100% satisfied so I looked for some examples and wrote my own script.

The addition of the tabbed layout is something that was not possible in awesome and I like that even more.

## Inspiration

I used most of the code from 
- https://github.com/TyberiusPrime/i3-instant-layout

Learned the basics from  
- https://github.com/olemartinorg/i3-alternating-layout

Throtteling
- https://stackoverflow.com/a/31923812